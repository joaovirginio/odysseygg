<?php

namespace Odyssey\CategoryGames\Setup;

use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

use Magento\Catalog\Model\CategoryFactory as Category;
use Magento\Catalog\Api\CategoryRepositoryInterface as Repository;

class InstallData implements InstallDataInterface
{

    private $categoryFactory;

    private $repository;

    public function __construct(Category $category, Repository $repository)
    {
        $this->categoryFactory = $category;
        $this->repository = $repository;
    }

    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        $data = $this->createCategory();

        foreach ($data as $d) {
            $validator = $this->categoryFactory->create();

            if (!$validator->loadByAttribute('url_key', $d['url_key'])) {
                $category = $this->categoryFactory->create();
                $category->setData($d);
                $this->save($category);
            }
        }

        $setup->endSetup();
    }

    private function createCategory()
    {
        $categories = array();

        $categories[] = [
            'name' => 'Games',
            'url_key' => 'games_odyssey',
            'active' => true,
            'is_anchor' => true,
            'include_in_menu' => true,
            'display_mode' => 'PRODUCTS_AND_PAGE',
            'is_active' => true,
            'parent_id' => 1
        ];

        return $categories;
    }

    private function save($category)
    {
        try {
            $this->repository->save($category);
        } catch (\Exception $exception) {
            echo $exception->getMessage();
        }
    }
}

?>