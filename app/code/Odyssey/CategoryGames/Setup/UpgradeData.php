<?php

namespace Odyssey\CategoryGames\Setup;

use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\UpgradeDataInterface;

use Magento\Catalog\Model\CategoryFactory as Category;
use Magento\Catalog\Api\CategoryRepositoryInterface as Repository;

class UpgradeData  implements UpgradeDataInterface
{
    private $categoryFactory;

    private $repository;

    public function __construct(Category $category, Repository $repository)
    {
        $this->categoryFactory = $category;
        $this->repository = $repository;
    }

    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context )
    {
        $setup->startSetup();

        if (version_compare($context->getVersion(), '1.1.1' , '<'))
        {
            $data = $this->createCategory();

            foreach ($data as $d) {
                $validator = $this->categoryFactory->create();

                if (!$validator->loadByAttribute('url_key', $d['url_key'])) {
                    $category = $this->categoryFactory->create();
                    $category->setData($d);
                    $this->save($category);
                }
            }
        }

        if (version_compare($context->getVersion(), '1.1.2' , '<'))
        {
            $data = $this->createCategory_Games();

            foreach ($data as $d) {
                $validator = $this->categoryFactory->create();

                if (!$validator->loadByAttribute('url_key', $d['url_key'])) {
                    $category = $this->categoryFactory->create();
                    $category->setData($d);
                    $this->save($category);
                }
            }
        }

        if (version_compare($context->getVersion(), '1.1.3' , '<'))
        {
            $data = $this->createCategory_Consoles();

            foreach ($data as $d) {
                $validator = $this->categoryFactory->create();

                if (!$validator->loadByAttribute('url_key', $d['url_key'])) {
                    $category = $this->categoryFactory->create();
                    $category->setData($d);
                    $this->save($category);
                }
            }
        }

        if (version_compare($context->getVersion(), '1.1.4' , '<'))
        {
            $data = $this->createCategory_Acessories();

            foreach ($data as $d) {
                $validator = $this->categoryFactory->create();

                if (!$validator->loadByAttribute('url_key', $d['url_key'])) {
                    $category = $this->categoryFactory->create();
                    $category->setData($d);
                    $this->save($category);
                }
            }
        }

        $setup->endSetup();
    }

    private function createCategory()
    {
        $parentCategory = $this->categoryFactory->create();

        $parentCategory = $parentCategory->loadByAttribute('url_key', 'games_odyssey');

        $categories = array();

        $categories[] = [
            'name' => 'Jogos',
            'url_key' => 'games',
            'active' => true,
            'is_anchor' => true,
            'include_in_menu' => true,
            'display_mode' => 'PRODUCTS_AND_PAGE',
            'is_active' => true,
            'parent_id' => $parentCategory->getId()
        ];

        $categories[] = [
            'name' => 'Consoles',
            'url_key' => 'consoles',
            'active' => true,
            'is_anchor' => true,
            'include_in_menu' => true,
            'display_mode' => 'PRODUCTS_AND_PAGE',
            'is_active' => true,
            'parent_id' => $parentCategory->getId()
        ];

        $categories[] = [
            'name' => 'Acessórios',
            'url_key' => 'acessories',
            'active' => true,
            'is_anchor' => true,
            'include_in_menu' => true,
            'display_mode' => 'PRODUCTS_AND_PAGE',
            'is_active' => true,
            'parent_id' => $parentCategory->getId()
        ];

        return $categories;
    }

    private function createCategory_Games()
    {
        $parentCategory = $this->categoryFactory->create();

        $parentCategory = $parentCategory->loadByAttribute('url_key', 'games');

        $categories = array();

        $categories[] = [
            'name' => 'Pc',
            'url_key' => 'pc_games',
            'active' => true,
            'is_anchor' => true,
            'include_in_menu' => true,
            'display_mode' => 'PRODUCTS_AND_PAGE',
            'is_active' => true,
            'parent_id' => $parentCategory->getId()
        ];

        $categories[] = [
            'name' => 'PS 4',
            'url_key' => 'ps4_games',
            'active' => true,
            'is_anchor' => true,
            'include_in_menu' => true,
            'display_mode' => 'PRODUCTS_AND_PAGE',
            'is_active' => true,
            'parent_id' => $parentCategory->getId()
        ];

        $categories[] = [
            'name' => 'XBOX One',
            'url_key' => 'xbox_games',
            'active' => true,
            'is_anchor' => true,
            'include_in_menu' => true,
            'display_mode' => 'PRODUCTS_AND_PAGE',
            'is_active' => true,
            'parent_id' => $parentCategory->getId()
        ];

        $categories[] = [
            'name' => 'Switch',
            'url_key' => 'switch_games',
            'active' => true,
            'is_anchor' => true,
            'include_in_menu' => true,
            'display_mode' => 'PRODUCTS_AND_PAGE',
            'is_active' => true,
            'parent_id' => $parentCategory->getId()
        ];

        return $categories;
    }

    private function createCategory_Consoles()
    {
        $parentCategory = $this->categoryFactory->create();

        $parentCategory = $parentCategory->loadByAttribute('url_key', 'consoles');

        $categories = array();

        $categories[] = [
            'name' => 'PS 4',
            'url_key' => 'ps4_consoles',
            'active' => true,
            'is_anchor' => true,
            'include_in_menu' => true,
            'display_mode' => 'PRODUCTS_AND_PAGE',
            'is_active' => true,
            'parent_id' => $parentCategory->getId()
        ];

        $categories[] = [
            'name' => 'XBOX One',
            'url_key' => 'xbox_consoles',
            'active' => true,
            'is_anchor' => true,
            'include_in_menu' => true,
            'display_mode' => 'PRODUCTS_AND_PAGE',
            'is_active' => true,
            'parent_id' => $parentCategory->getId()
        ];

        $categories[] = [
            'name' => 'Switch',
            'url_key' => 'switch_consoles',
            'active' => true,
            'is_anchor' => true,
            'include_in_menu' => true,
            'display_mode' => 'PRODUCTS_AND_PAGE',
            'is_active' => true,
            'parent_id' => $parentCategory->getId()
        ];

        return $categories;
    }

    private function createCategory_Acessories()
    {
        $parentCategory = $this->categoryFactory->create();

        $parentCategory = $parentCategory->loadByAttribute('url_key', 'acessories');

        $categories = array();

        $categories[] = [
            'name' => 'Fones',
            'url_key' => 'headsets',
            'active' => true,
            'is_anchor' => true,
            'include_in_menu' => true,
            'display_mode' => 'PRODUCTS_AND_PAGE',
            'is_active' => true,
            'parent_id' => $parentCategory->getId()
        ];

        $categories[] = [
            'name' => 'Teclados',
            'url_key' => 'keyboards',
            'active' => true,
            'is_anchor' => true,
            'include_in_menu' => true,
            'display_mode' => 'PRODUCTS_AND_PAGE',
            'is_active' => true,
            'parent_id' => $parentCategory->getId()
        ];

        $categories[] = [
            'name' => 'Mouses',
            'url_key' => 'mouses',
            'active' => true,
            'is_anchor' => true,
            'include_in_menu' => true,
            'display_mode' => 'PRODUCTS_AND_PAGE',
            'is_active' => true,
            'parent_id' => $parentCategory->getId()
        ];

        $categories[] = [
            'name' => 'Controles',
            'url_key' => 'joysticks',
            'active' => true,
            'is_anchor' => true,
            'include_in_menu' => true,
            'display_mode' => 'PRODUCTS_AND_PAGE',
            'is_active' => true,
            'parent_id' => $parentCategory->getId()
        ];

        return $categories;
    }

    private function save($category)
    {
        try {
            $this->repository->save($category);
        } catch (\Exception $exception) {
            echo $exception->getMessage();
        }
    }
}

?>