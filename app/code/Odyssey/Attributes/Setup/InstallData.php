<?php

namespace Odyssey\Attributes\Setup;

use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

use Magento\Eav\Setup\EavSetupFactory;

use Magento\Catalog\Model\Product;

use Magento\Eav\Model\ResourceModel\Entity\Attribute;

class InstallData implements InstallDataInterface
{
    private $eavSetupFactory;

    public function __construct(EavSetupFactory $eavSetupFactory, Attribute $eavAttribute
    )
    {
        $this->eavSetupFactory = $eavSetupFactory;
        $this->eavAttribute = $eavAttribute;
    }

    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);

        $eavSetup->addAttribute(
            Product::ENTITY,
            'size',
            [
                'type' => 'text',
                'label' => 'Tamanho',
                'input' => 'select',
                'required' => false,
                'source' => \Magento\Eav\Model\Entity\Attribute\Source\Table::class,
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                'user_defined' => true,
                'visible' => true,
                'group' => 'Geek',
                'sort_order' => 50,
                'visible_on_front' => true,
                'attribute_set_id' => 'Geek'
            ]
        );

        $sizeID = $this->eavAttribute   ->getIdByCode(\Magento\Catalog\Model\Product::ENTITY, 'size');

        $sizes = [
            "PP",
            "P",
            "M",
            "G",
            "GG",
            "altura 25.8 cm X 14.8 cm comprimento",
            "altura 35.8 cm X 24.8 cm comprimento",
            "altura 45.8 cm X 34.8 cm comprimento",
            "comprimento 10 cm  X  11 cm largura"
        ];

        $sizeOptions['attribute_id'] = $sizeID;
        foreach ($sizes as $size => $value) {
            $sizeOptions['value'][$value][0] = $value;
        }

        $eavSetup->addAttributeOption($sizeOptions);

        $eavSetup->addAttribute(
            Product::ENTITY,
            'height',
            [
                'type' => 'text',
                'label' => 'Altura',
                'input' => 'text',
                'required' => false,
                'source' => \Magento\Eav\Model\Entity\Attribute\Source\Table::class,
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                'user_defined' => true,
                'visible' => true,
                'group' => 'Geek',
                'sort_order' => 51,
                'visible_on_front' => true,
                'attribute_set_id' => 'Geek'
            ]
        );

        $eavSetup->addAttribute(
            Product::ENTITY,
            'gender',
            [
                'type' => 'text',
                'label' => 'Gênero',
                'input' => 'select',
                'required' => false,
                'source' => \Magento\Eav\Model\Entity\Attribute\Source\Table::class,
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                'user_defined' => true,
                'visible' => true,
                'group' => 'Geek',
                'sort_order' => 52,
                'visible_on_front' => true,
                'attribute_set_id' => 'Geek'
            ]
        );

        $genderID = $this->eavAttribute->getIdByCode(\Magento\Catalog\Model\Product::ENTITY, 'gender');

        $genders = [
            "Feminino",
            "Masculino"
        ];

        $genderOptions['attribute_id'] = $genderID;
        foreach ($genders as $gender => $value) {
            $genderOptions['value'][$value][0] = $value;
        }

        $eavSetup->addAttributeOption($genderOptions);

        $eavSetup->addAttribute(
            Product::ENTITY,
            'model',
            [
                'type' => 'text',
                    'label' => 'Modelo',
                'input' => 'text',
                'required' => false,
                'source' => \Magento\Eav\Model\Entity\Attribute\Source\Table::class,
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                'user_defined' => true,
                'visible' => true,
                'group' => 'Games',
                'sort_order' => 53,
                'visible_on_front' => true,
                'attribute_set_id' => 'Games'
            ]
        );

        $eavSetup->addAttribute(
            Product::ENTITY,
            'year',
            [
                'type' => 'text',
                'label' => 'Ano',
                'input' => 'text',
                'required' => false,
                'source' => \Magento\Eav\Model\Entity\Attribute\Source\Table::class,
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                'user_defined' => true,
                'visible' => true,
                'group' => 'Games',
                'sort_order' => 54,
                'visible_on_front' => true,
                'attribute_set_id' => 'Games'
            ]
        );

        $eavSetup->addAttribute(
            Product::ENTITY,
            'genre',
            [
                'type' => 'text',
                'label' => 'Gênero',
                'input' => 'text',
                'required' => false,
                'source' => \Magento\Eav\Model\Entity\Attribute\Source\Table::class,
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                'user_defined' => true,
                'visible' => true,
                'group' => 'Games',
                'sort_order' => 55,
                'visible_on_front' => true,
                'attribute_set_id' => 'Games'
            ]
        );

        $eavSetup->addAttribute(
            Product::ENTITY,
            'type',
            [
                'type' => 'text',
                'label' => 'Tipo',
                'input' => 'text',
                'required' => false,
                'source' => \Magento\Eav\Model\Entity\Attribute\Source\Table::class,
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                'user_defined' => true,
                'visible' => true,
                'group' => 'Games',
                'sort_order' => 56,
                'visible_on_front' => true,
                'attribute_set_id' => 'Games'
            ]
        );

        $eavSetup->addAttribute(
            Product::ENTITY,
            'distributor',
            [
                'type' => 'text',
                'label' => 'Distribuidora',
                'input' => 'text',
                'required' => false,
                'source' => \Magento\Eav\Model\Entity\Attribute\Source\Table::class,
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                'user_defined' => true,
                'visible' => true,
                'group' => 'Games',
                'sort_order' => 57,
                'visible_on_front' => true,
                'attribute_set_id' => 'Games'
            ]
        );

        $eavSetup->addAttribute(
            Product::ENTITY,
            'developer',
            [
                'type' => 'text',
                'label' => 'Desenvolvedora',
                'input' => 'text',
                'required' => false,
                'source' => \Magento\Eav\Model\Entity\Attribute\Source\Table::class,
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                'user_defined' => true,
                'visible' => true,
                'group' => 'Games',
                'sort_order' => 58,
                'visible_on_front' => true,
                'attribute_set_id' => 'Games'
            ]
        );

        $eavSetup->addAttribute(
            Product::ENTITY,
            'color',
            [
                'type' => 'text',
                'label' => 'Cor',
                'input' => 'text',
                'required' => false,
                'source' => \Magento\Eav\Model\Entity\Attribute\Source\Table::class,
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                'user_defined' => true,
                'visible' => true,
                'group' => 'Games',
                'sort_order' => 59,
                'visible_on_front' => true,
                'attribute_set_id' => 'Games'
            ]
        );

        $eavSetup->addAttribute(
            Product::ENTITY,
            'brand',
            [
                'type' => 'text',
                'label' => 'Marca',
                'input' => 'text',
                'required' => false,
                'source' => \Magento\Eav\Model\Entity\Attribute\Source\Table::class,
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                'user_defined' => true,
                'visible' => true,
                'group' => 'Games',
                'sort_order' => 60,
                'visible_on_front' => true
            ]
        );
    }
}