<?php

namespace Odyssey\CreatingStores\Setup;

use Magento\Framework\Event\ManagerInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\UpgradeDataInterface;

use Magento\Store\Model\WebsiteFactory;
use Magento\Store\Model\ResourceModel\Website;

use Magento\Store\Model\GroupFactory;
use Magento\Store\Model\ResourceModel\Group;

use Magento\Store\Model\StoreFactory;
use Magento\Store\Model\ResourceModel\Store;

use Magento\Catalog\Model\CategoryFactory as Category;

use Magento\Theme\Model\ResourceModel\Theme\CollectionFactory;

use Magento\Framework\App\Config\ConfigResource\ConfigInterface;

class UpgradeData implements UpgradeDataInterface
{
    private $websiteFactory;
    private $websiteResourceModel;

    private $groupFactory;
    private $groupResourceModel;

    private $storeFactory;
    private $storeResourceModel;

    private $eventManager;

    private $categoryFactory;

    private $collectionFactory;

    private $configInterface;

    public function __construct(
        WebsiteFactory $websiteFactory,
        Website $websiteResourceModel,
        GroupFactory $groupFactory,
        Group $groupResourceModel,
        StoreFactory $storeFactory,
        Store $storeResourceModel,
        ManagerInterface $eventManager,
        Category $category,
        CollectionFactory $collectionFactory,
        ConfigInterface $configInterface
    )
    {
        $this->websiteFactory = $websiteFactory;
        $this->websiteResourceModel = $websiteResourceModel;

        $this->groupFactory = $groupFactory;
        $this->groupResourceModel = $groupResourceModel;

        $this->storeFactory = $storeFactory;
        $this->storeResourceModel = $storeResourceModel;

        $this->eventManager = $eventManager;

        $this->categoryFactory = $category;

        $this->collectionFactory = $collectionFactory;

        $this->configInterface = $configInterface;
    }

    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        if (version_compare($context->getVersion(), '1.1.1', '<'))
        {
            $this->CreateWebSite();
        }

        if (version_compare($context->getVersion(), '1.1.2', '<'))
        {
            $this->CreateGeekGroup();
            $this->CreateGamesGroup();
        }

        if (version_compare($context->getVersion(), '1.1.3', '<'))
        {
            $this->CreateGeekStore();
            $this->CreateGamesStore();
        }

        if (version_compare($context->getVersion(), '1.1.4', '<'))
        {
            $this->GeekTheme();
            $this->GamesTheme();
        }

        $setup->endSetup();
    }

    private function CreateWebSite()
    {
        $website = $this->websiteFactory->create();

        $website->setCode('odyssey');
        $website->setName('Odyssey');
        $website->setDefaultGroupId(1);
        $website->setName('Odyssey');
        $website->setIsDefault(1);

        $this->websiteResourceModel->save($website);

        $this->websiteID = $website->getId();
    }

    private function CreateGeekGroup()
    {
        $parentCategory = $this->categoryFactory->create();

        $parentCategory = $parentCategory->loadByAttribute('url_key', 'geek_odyssey');


        $group = $this->groupFactory->create();

        $group->setWebsiteId($this->websiteID); //
        $group->setCode('geek_group_store');
        $group->setName('Odyssey Geek');
        $group->setRootCategoryId($parentCategory->getId());

        $this->groupResourceModel->save($group);

        $this->groupGeekID = $group->getId();
    }

    private function CreateGamesGroup()
    {
        $parentCategory = $this->categoryFactory->create();

        $parentCategory = $parentCategory->loadByAttribute('url_key', 'games_odyssey');


        $group = $this->groupFactory->create();

        $group->setWebsiteId($this->websiteID); //
        $group->setCode('games_group_store');
        $group->setName('Odyssey Games');
        $group->setRootCategoryId($parentCategory->getId());

        $this->groupResourceModel->save($group);

        $this->groupGamesID = $group->getId();
    }

    private function CreateGeekStore()
    {
        $store = $this->storeFactory->create();

        $store->setCode('odyssey_geek');
        $store->setName('Odyssey Geek View');
        $store->setGroupId($this->groupGeekID);
        $store->setWebsiteId($this->websiteID); //
        $store->setData('is_active', '1');

        $this->storeResourceModel->save($store);

        $this->GeekID = $store->getId();
    }

    private function CreateGamesStore()
    {
        $store = $this->storeFactory->create();

        $store->setCode('odyssey_games');
        $store->setName('Odyssey Games View');
        $store->setGroupId($this->groupGamesID);
        $store->setWebsiteId($this->websiteID); //
        $store->setData('is_active', '1');

        $this->storeResourceModel->save($store);

        $this->GamesID = $store->getId();
    }

    private function GeekTheme()
    {
        $themes = $this->collectionFactory->create()->loadRegisteredThemes();

        foreach ($themes as $theme)
        {
            if ($theme->getCode() == 'Odyssey/geek')
            {
                $this->configInterface->saveConfig(
                    'design/theme/theme_id', $theme->getId(),
                    'stores', $this->GeekID
                    );
            }
        }
    }

    private function GamesTheme()
    {
        $themes = $this->collectionFactory->create()->loadRegisteredThemes();

        foreach ($themes as $theme)
        {
            if ($theme->getCode() == 'Odyssey/games')
            {
                $this->configInterface->saveConfig(
                    'design/theme/theme_id', $theme->getId(),
                    'stores', $this->GamesID
                );
            }
        }
    }
}

?>