<?php

namespace Odyssey\CategoryGeek\Setup;

use Magento\Catalog\Helper\DefaultCategory;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\UpgradeDataInterface;

use Magento\Catalog\Model\CategoryFactory as Category;
use Magento\Catalog\Api\CategoryRepositoryInterface as Repository;

class UpgradeData  implements UpgradeDataInterface
{
    private $categoryFactory;

    public function __construct(Category $category, Repository $repository, DefaultCategory $defaultCategoryHelper)
    {
        $this->categoryFactory = $category;
        $this->repository = $repository;
        $this->defaultCategoryHelper = $defaultCategoryHelper;
    }

    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context )
    {
        $setup->startSetup();

        if (version_compare($context->getVersion(), '1.1.1' , '<'))
        {
            $data = $this->createCategory();

            foreach ($data as $d) {
                $validator = $this->categoryFactory->create();

                if (!$validator->loadByAttribute('url_key', $d['url_key'])) {
                    $category = $this->categoryFactory->create();
                    $category->setData($d);
                    $this->save($category);
                }
            }
        }

        if (version_compare($context->getVersion(), '1.1.2' , '<'))
        {
            $data = $this->createCategory_T_shirts();


            foreach ($data as $d) {
                $validator = $this->categoryFactory->create();

                if (!$validator->loadByAttribute('url_key', $d['url_key'])) {
                    $category = $this->categoryFactory->create();
                    $category->setData($d);
                    $this->save($category);
                }
            }
        }

        if (version_compare($context->getVersion(), '1.1.3' , '<'))
        {
            $data = $this->createCategory_Long_T_shirts();


            foreach ($data as $d) {
                $validator = $this->categoryFactory->create();

                if (!$validator->loadByAttribute('url_key', $d['url_key'])) {
                    $category = $this->categoryFactory->create();
                    $category->setData($d);
                    $this->save($category);
                }
            }
        }

        if (version_compare($context->getVersion(), '1.1.4' , '<'))
        {
            $data = $this->createCategory_Blouses();

            foreach ($data as $d) {
                $validator = $this->categoryFactory->create();

                if (!$validator->loadByAttribute('url_key', $d['url_key'])) {
                    $category = $this->categoryFactory->create();
                    $category->setData($d);
                    $this->save($category);
                }
            }
        }

        if (version_compare($context->getVersion(), '1.1.5' , '<'))
        {
            $data = $this->createCategory_Posters();

            foreach ($data as $d) {
                $validator = $this->categoryFactory->create();

                if (!$validator->loadByAttribute('url_key', $d['url_key'])) {
                    $category = $this->categoryFactory->create();
                    $category->setData($d);
                    $this->save($category);
                }
            }
        }

        $setup->endSetup();
    }

    private function createCategory()
    {
        $parentCategory = $this->categoryFactory->create();

        $parentCategory = $parentCategory->loadByAttribute('url_key', 'geek_odyssey');

        $categories = array();

        $categories[] = [
            'name' => 'Camisetas',
            'url_key' => 't_shirts',
            'active' => true,
            'is_anchor' => true,
            'include_in_menu' => true,
            'display_mode' => 'PRODUCTS_AND_PAGE',
            'is_active' => true,
            'parent_id' => $parentCategory->getId()
        ];

        $categories[] = [
            'name' => 'Camisetas de Manga Longa',
            'url_key' => 'long_t_shirts',
            'active' => true,
            'is_anchor' => true,
            'include_in_menu' => true,
            'display_mode' => 'PRODUCTS_AND_PAGE',
            'is_active' => true,
            'parent_id' => $parentCategory->getId()
        ];

        $categories[] = [
            'name' => 'Blusas',
            'url_key' => 'blouses',
            'active' => true,
            'is_anchor' => true,
            'include_in_menu' => true,
            'display_mode' => 'PRODUCTS_AND_PAGE',
            'is_active' => true,
            'parent_id' => $parentCategory->getId()
        ];

        $categories[] = [
            'name' => 'Action Figures',
            'url_key' => 'action_figures',
            'active' => true,
            'is_anchor' => true,
            'include_in_menu' => true,
            'display_mode' => 'PRODUCTS_AND_PAGE',
            'is_active' => true,
            'parent_id' => $parentCategory->getId()
        ];

        $categories[] = [
            'name' => 'Pôsters com Moldura',
            'url_key' => 'posters',
            'active' => true,
            'is_anchor' => true,
            'include_in_menu' => true,
            'display_mode' => 'PRODUCTS_AND_PAGE',
            'is_active' => true,
            'parent_id' => $parentCategory->getId()
        ];

        $categories[] = [
            'name' => 'Canecas',
            'url_key' => 'mugs',
            'active' => true,
            'is_anchor' => true,
            'include_in_menu' => true,
            'display_mode' => 'PRODUCTS_AND_PAGE',
            'is_active' => true,
            'parent_id' => $parentCategory->getId()
        ];

        return $categories;
    }

    private function createCategory_T_shirts()
    {
        $parentCategory = $this->categoryFactory->create();

        $parentCategory = $parentCategory->loadByAttribute('url_key', 't_shirts');

        $categories = array();

        $categories[] = [
            'name' => 'Series e Filmes',
            'url_key' => 'series_movies_t_shirts',
            'active' => true,
            'is_anchor' => true,
            'include_in_menu' => true,
            'display_mode' => 'PRODUCTS_AND_PAGE',
            'is_active' => true,
            'parent_id' => $parentCategory->getId()
        ];

        $categories[] = [
            'name' => 'Animes',
            'url_key' => 'anime_t_shirts',
            'active' => true,
            'is_anchor' => true,
            'include_in_menu' => true,
            'display_mode' => 'PRODUCTS_AND_PAGE',
            'is_active' => true,
            'parent_id' => $parentCategory->getId()
        ];

        $categories[] = [
            'name' => 'Jogos',
            'url_key' => 'games_t_shirts',
            'active' => true,
            'is_anchor' => true,
            'include_in_menu' => true,
            'display_mode' => 'PRODUCTS_AND_PAGE',
            'is_active' => true,
            'parent_id' => $parentCategory->getId()
        ];

        return $categories;
    }

    private function createCategory_Long_T_shirts()
    {
        $parentCategory = $this->categoryFactory->create();

        $parentCategory = $parentCategory->loadByAttribute('url_key', 'long_t_shirts');

        $categories = array();

        $categories[] = [
            'name' => 'Series e Filmes',
            'url_key' => 'series_movies_long_t_shirts',
            'active' => true,
            'is_anchor' => true,
            'include_in_menu' => true,
            'display_mode' => 'PRODUCTS_AND_PAGE',
            'is_active' => true,
            'parent_id' => $parentCategory->getId()
        ];

        $categories[] = [
            'name' => 'Animes',
            'url_key' => 'anime_long_t_shirts',
            'active' => true,
            'is_anchor' => true,
            'include_in_menu' => true,
            'display_mode' => 'PRODUCTS_AND_PAGE',
            'is_active' => true,
            'parent_id' => $parentCategory->getId()
        ];

        $categories[] = [
            'name' => 'Jogos',
            'url_key' => 'games_long_t_shirts',
            'active' => true,
            'is_anchor' => true,
            'include_in_menu' => true,
            'display_mode' => 'PRODUCTS_AND_PAGE',
            'is_active' => true,
            'parent_id' => $parentCategory->getId()
        ];

        return $categories;
    }

    private function createCategory_Blouses()
    {
        $parentCategory = $this->categoryFactory->create();

        $parentCategory = $parentCategory->loadByAttribute('url_key', 'blouses');

        $categories = array();

        $categories[] = [
            'name' => 'Series e Filmes',
            'url_key' => 'series_movies_blouses',
            'active' => true,
            'is_anchor' => true,
            'include_in_menu' => true,
            'display_mode' => 'PRODUCTS_AND_PAGE',
            'is_active' => true,
            'parent_id' => $parentCategory->getId()
        ];

        $categories[] = [
            'name' => 'Animes',
            'url_key' => 'anime_blouses',
            'active' => true,
            'is_anchor' => true,
            'include_in_menu' => true,
            'display_mode' => 'PRODUCTS_AND_PAGE',
            'is_active' => true,
            'parent_id' => $parentCategory->getId()
        ];

        $categories[] = [
            'name' => 'Jogos',
            'url_key' => 'games_blouses',
            'active' => true,
            'is_anchor' => true,
            'include_in_menu' => true,
            'display_mode' => 'PRODUCTS_AND_PAGE',
            'is_active' => true,
            'parent_id' => $parentCategory->getId()
        ];

        return $categories;
    }

    private function createCategory_Posters()
    {
        $parentCategory = $this->categoryFactory->create();

        $parentCategory = $parentCategory->loadByAttribute('url_key', 'posters');

        $categories = array();

        $categories[] = [
            'name' => 'Series e Filmes',
            'url_key' => 'series_movies_posters',
            'active' => true,
            'is_anchor' => true,
            'include_in_menu' => true,
            'display_mode' => 'PRODUCTS_AND_PAGE',
            'is_active' => true,
            'parent_id' => $parentCategory->getId()
        ];

        $categories[] = [
            'name' => 'Animes',
            'url_key' => 'anime_posters',
            'active' => true,
            'is_anchor' => true,
            'include_in_menu' => true,
            'display_mode' => 'PRODUCTS_AND_PAGE',
            'is_active' => true,
            'parent_id' => $parentCategory->getId()
        ];

        $categories[] = [
            'name' => 'Jogos',
            'url_key' => 'games_posters',
            'active' => true,
            'is_anchor' => true,
            'include_in_menu' => true,
            'display_mode' => 'PRODUCTS_AND_PAGE',
            'is_active' => true,
            'parent_id' => $parentCategory->getId()
        ];

        return $categories;
    }

    private function save($category)
    {
        try {
            $this->repository->save($category);
        } catch (\Exception $exception) {
            echo $exception->getMessage();
        }
    }
}

?>