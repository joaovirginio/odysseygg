bin/magento deploy:mode:set developer;
bin/magento cron:run;
bin/magento configurator:run --env="1";
bin/magento setup:static-content:deploy en_US pt_BR -f;
bin/magento cache:flush;
bin/magento indexer:reindex;