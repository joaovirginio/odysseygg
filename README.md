##### **Loja e-commerce utilizando Magento**

Projeto de 2 lojas de e-commerce utilizando o framework Magento para finalizar o aprendizado. Projeto destinado ao treinamento do time de estágiarios da WebJump!

#####Time Odyssey

    • Adison Rocha
    • João Vitor Virginio
    • Luiz Felipe Silva 
     


Dados do Ambiente

    •   Magento Versão 2.3.3
    •   Php 7.2.19
    •   Banco MySQL Ver 14.14 Disrib 5.7.28
    •   Nginx 1.14.0
    •   Ubuntu 18.04.3 LTS
    •   COmposer 1.6.3
    
    
###Instalação
   
   • Realize um git clone do repositório
   
      git clone git@bitbucket.org:joaovirginio/odysseygg.git <NOME DA PASTA>

          
   Logo após
	 
    • Execute o comando 
		
		composer install

   • Instalar dados do magento no banco:
       
       Já tendo um schema vazio no seu banco de dados preparado para receber o magento..
       
       • A instalação pode ser feita pelo Browser acesse o link abaixo para mais informações:
        
            https://devdocs.magento.com/guides/v2.3/install-gde/composer.html
        
        
       • Pode ser feita por comando também:
            
            bin/magento setup:install \
            --base-url=http://odyssey.develop \
            --db-host=localhost \
            --db-name=magento \
            --db-user=seuuser \
            --db-password=suasenha \
            --admin-firstname=admin \
            --admin-lastname=admin \
            --admin-email=admin@admin.com \
            --admin-user=admin \
            --admin-password=admin123 \
            --language=pt_BR \
            --currency=BRL \
            --timezone=America/Sao_Paulo \
            --use-rewrites=1
            
            (Colocar nos campos "db-name", "db-user", "db-password" as informações referentes ao que esta no seu banco de dados)
          
  • Então execute o comando a seguir , que ira criar os produtos e setar algumas configurações no admin.   
	
	./commands.sh 

    Este arquivo commands.sh executa os seguintes comandos:
  
        bin/magento deploy:mode:set developer;
        bin/magento cron:run;
        bin/magento configurator:run --env="1";
        setup:static-content:deploy -f pt_BR en_US;
        bin/magento cache:flush;
        bin/magento indexer:reindex;